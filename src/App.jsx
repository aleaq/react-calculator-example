import { useState } from 'react';
import * as math from 'mathjs/lib/browser/math.js';

const Calculator = () => {
  const numberInput = (e) => {
    const number = e.target.value;
    setInputVal(inputVal + number);
    e.target.blur();
  };

  const clear = (e) => {
    setInputVal('');
    e.target.blur();
  };

  const calculateTotal = (e) => {
    const total = math.compile(inputVal);
    const result = total.evaluate();
    setInputVal(Math.round(result * 10000) / 10000);
    e.target.blur();
  };

  const squareRoot = (e) => {
    const total = parseInt(inputVal);
    const sqr = math.sqrt(total);
    setInputVal(Math.round(sqr * 10000) / 10000);
    e.target.blur();
  };

  const [inputVal, setInputVal] = useState('');
  const [isActive, setIsActive] = useState(false);

  return (
    <div className='calculator'>
      <div className='calculator-grid'>
        <div className='screen'>
          <div className='screen-input'>
            <input readOnly={true} type='text' value={inputVal} />
          </div>
        </div>
        <div className='calc-body'>
          <button onClick={numberInput} className='secondary-btn' value='/'>
            ÷
          </button>
          <button onClick={numberInput} className='primary-btn' value='7'>
            7
          </button>
          <button onClick={numberInput} className='primary-btn' value='8'>
            8
          </button>
          <button onClick={numberInput} className='primary-btn' value='9'>
            9
          </button>
          <button onClick={numberInput} className='secondary-btn' value='*'>
            x
          </button>
          <button onClick={numberInput} className='secondary-btn' value='%'>
            %
          </button>
          <button onClick={numberInput} className='primary-btn' value='4'>
            4
          </button>
          <button onClick={numberInput} className='primary-btn' value='5'>
            5
          </button>
          <button onClick={numberInput} className='primary-btn' value='6'>
            6
          </button>
          <button onClick={numberInput} className='secondary-btn' value='-'>
            -
          </button>
          <button onClick={squareRoot} className='secondary-btn' value='√'>
            √
          </button>
          <button onClick={numberInput} className='primary-btn' value='1'>
            1
          </button>
          <button onClick={numberInput} className='primary-btn' value='2'>
            2
          </button>
          <button onClick={numberInput} className='primary-btn' value='3'>
            3
          </button>
          <button onClick={numberInput} className='secondary-btn plus-btn' value='+'>
            +
          </button>
          <button onClick={clear} className='accent-btn'>
            AC
          </button>
          <button onClick={numberInput} className='primary-btn' value='0'>
            0
          </button>
          <button onClick={numberInput} className='primary-btn' value='.'>
            .
          </button>
          <button onClick={calculateTotal} className='secondary-btn'>
            =
          </button>
        </div>
      </div>
    </div>
  );
};

export default Calculator;
