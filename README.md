# React calculator example

![calculator from this project](./public/images/calc-img.png)

## About this project

I wanted to create a quick basic example of how to create a calculator with
the package [Math.js](https://mathjs.org/).

I decided to recreate the style of a calculator I remember my parents having when I was a child.

## How this was made

This project was created with:

- React
- Vite
- Custom SCSS/CSS styles
- Math.js
- Font style - Digital

## How to run this project

Pull down the project and run `yarn` to install dependencies.
To run the project type `yarn dev` into your terminal in the root of the project.

Note: This is a basic example of a calculator. Feel free to use to learn! This example is not optimized for mobile. 
